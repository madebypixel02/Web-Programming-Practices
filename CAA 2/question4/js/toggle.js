class Toggle {
    #ref;
    #refStatus;
    #status;
  
    constructor(refDom) {
      this.#ref = refDom;
      this.init();
    }
  
    init() {
      this.#status = this.#ref.dataset.status === 'true';
      this.#refStatus = this.#ref.querySelector('.js-toggle-status');
      this.#ref.addEventListener('click', this.onClick.bind(this));
      this.render();
    }
  
    onClick() {
      this.#status = !this.#status;
      this.render();
    }
  
    render() {
      this.#ref.dataset.status = this.#status;
      this.#refStatus.textContent = this.#status ? 'ON' : 'OFF';
      this.#refStatus.classList.toggle('toggle__status--on', this.#status);
    }
  }
  
  export default Toggle;