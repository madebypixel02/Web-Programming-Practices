import ListItem from './list-item.js';

class FilterList {
    constructor(listSelector, searchSelector, dataset) {
        this.refList = document.querySelector(listSelector);
        this.refSearch = document.querySelector(searchSelector);
        this.items = [];
        this.dataset = dataset;

        this.init();
    }

    init() {
        this.dataset.forEach(data => {
            this.items.push(new ListItem(data));
        });

        this.refSearch.addEventListener('keyup', this.onSearchChanged.bind(this));

        this.render();
    }

    render() {
        const searchTerm = this.refSearch.value;
        const filteredItems = this.items.filter(item => item.match(searchTerm));

        this.refList.innerHTML = filteredItems.map(item => item.render()).join('');
    }

    onSearchChanged(ev) {
        this.render();
    }
}

export default FilterList;