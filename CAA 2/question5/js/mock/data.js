export const LIST_DATA = [
    {
        name: {
            first: 'Bertram',
            last: 'Mueller',
        },
        jobTitle: 'Forward Factors Designer',
        email: 'Cierra18@example.com',
    },
    {
        name: {
            first: 'Pink',
            last: 'Kunze',
        },
        jobTitle: 'National Implementation Consultant',
        email: 'Elmore.Padberg97@example.com',
    },
    {
        name: {
            first: 'Chad',
            last: 'Luettgen',
        },
        jobTitle: 'Investor Markets Analyst',
        email: 'Mateo28@example.com',
    },
    {
        name: {
            first: 'Baby',
            last: 'Ziemann',
        },
        jobTitle: 'Future Accountability Consultant',
        email: 'Donnell.Zboncak5@example.com',
    },
    {
        name: {
            first: 'Brock',
            last: 'Conroy',
        },
        jobTitle: 'Central Directives Officer',
        email: 'Margarita26@example.com',
    },
    {
        name: {
            first: 'Laurie',
            last: 'Glover',
        },
        jobTitle: 'Lead Marketing Agent',
        email: 'Sandra_Tremblay61@example.com',
    },
    {
        name: {
            first: 'Leopoldo',
            last: 'Halvorson',
        },
        jobTitle: 'National Communications Associate',
        email: 'Cara.Schuppe@example.com',
    },
    {
        name: {
            first: 'Melvina',
            last: 'Wolf',
        },
        jobTitle: 'Global Infrastructure Developer',
        email: 'Gladyce_OKon@gmail.com',
    },
    {
        name: {
            first: 'Verner',
            last: 'Hartmann',
        },
        jobTitle: 'Product Tactics Representative',
        email: 'Pattie94@example.com',
    },
    {
        name: {
            first: 'Damon',
            last: 'Considine',
        },
        jobTitle: 'Legacy Paradigm Coordinator',
        email: 'Irwin.Thiel@example.com',
    },
    {
        name: {
            first: 'Winfield',
            last: 'Huel',
        },
        jobTitle: 'Product Creative Designer',
        email: 'Timmy.Abshire@example.com',
    },
    {
        name: {
            first: 'Eldon',
            last: "D'Amore",
        },
        jobTitle: 'Lead Directives Executive',
        email: 'Hailey48@example.com',
    },
    {
        name: {
            first: 'Adalberto',
            last: 'Corwin',
        },
        jobTitle: 'Product Assurance Assistant',
        email: 'Linnie_Mayert@example.com',
    },
    {
        name: {
            first: 'Adrienne',
            last: 'Mann',
        },
        jobTitle: 'Regional Identity Officer',
        email: 'Gust_Rodriguez54@gmail.com',
    },
    {
        name: {
            first: 'Gustave',
            last: 'Klein',
        },
        jobTitle: 'Central Security Consultant',
        email: 'Leonie.Wisoky@example.com',
    },
    {
        name: {
            first: 'Gordon',
            last: 'Mills',
        },
        jobTitle: 'Forward Mobility Liaison',
        email: 'Deanna_Cole5@example.com',
    },
    {
        name: {
            first: 'Robbie',
            last: 'Leuschke',
        },
        jobTitle: 'Legacy Integration Supervisor',
        email: 'Jennyfer16@gmail.com',
    },
    {
        name: {
            first: 'Mac',
            last: 'Reinger',
        },
        jobTitle: 'Legacy Optimization Producer',
        email: 'Sandrine_Walsh94@example.com',
    },
    {
        name: {
            first: 'Celestine',
            last: 'Howell',
        },
        jobTitle: 'Global Paradigm Analyst',
        email: 'Lois.Quitzon@example.com',
    },
    {
        name: {
            first: 'Shanna',
            last: 'Stoltenberg',
        },
        jobTitle: 'Principal Accountability Director',
        email: 'Garth36@example.com',
    },
];
