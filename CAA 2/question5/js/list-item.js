class ListItem {
    constructor(data) {
        this.data = data;
    }

    match(searchTerm) {
        // Convert data properties to a string and make it lowercase for case-insensitive search
        const dataString = JSON.stringify(this.data).toLowerCase();
        return dataString.includes(searchTerm.toLowerCase());
    }

    render() {
        return `
            <li class="contactlist__item contact">
                <div>
                    <p class="contact__name"><strong>${this.data.name.last}, ${this.data.name.first}</strong></p>
                    <p class="contact__job">${this.data.jobTitle}</p>
                    <p class="contact__email">
                        <a class="contact__email-link" href="mailto: ${this.data.email}">${this.data.email}</a>
                    </p>
                </div>
            </li>
        `;
    }
}

export default ListItem;