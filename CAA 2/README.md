# CAA 2
Alejandro Pérez Bueno
May 16, 2024

- [Question 1](#question-1)
- [Question 2](#question-2)
- [Question 3](#question-3)
- [Question 4](#question-4)
- [Question 5](#question-5)



## Question 1

The answer is
`b: export default can only be used once in a module, while export can be used multiple times`.

- `export default` is designed to export a single primary value from a
  module, such as a function, class or object.
- `export` (AKA named export) is used to export multiple named values
  from a module.

## Question 2

The answer is
`c: createElement creates a single element, while createDocumentFragment creates a temporary container for multiple elements`.

- `createElement` is used to create a new *HTML* element. You specify
  the type of element you want (e.g. `div`, `p`, `span`) as an argument.
  This new element is not immediately part of the *DOM*, you need to
  append it to an existing DOM node to make it visible on the page.
- `createDocumentFragment` creates a virtual, lightweight container that
  is not part of the actual *DOM* tree. You can add multiple elements to
  this fragment, and then append the entire fragment to the *DOM*. This
  is much more efficient than adding each element individually, as it
  minimizes *DOM* manipulations and reflows.

## Question 3

Here is the full code and its explanation:

``` js
function filterItems() {
  // a. Select all DOM nodes with class "item" inside a node with class "list"
  const items = document.querySelectorAll('.list .item');

  // Iterate through each item to apply the contditions
  items.forEach(item => {
    // b. Hide items with even data-id
    const itemId = parseInt(item.getAttribute('data-id'));
    if (itemId % 2 === 0) {
      item.style.display = 'none';
    }

    // c. Hide items whose data-name doesn't start with "f" (case-insensitive)
    const itemName = item.getAttribute('data-name');
    if (!itemName.toLowerCase().startsWith('f')) {
      item.style.display = 'none';
    }

    // d. Fill text content with data-name in another case
    const currentCase = \
      itemName === itemName.toLowerCase() ? 'lowercase' : 'uppercase';
    const newCase = currentCase === 'lowercase' ? 'uppercase' : 'lowercase';
    item.textContent = \
      newCase === 'uppercase' ? itemName.toUpperCase() : itemName.toLowerCase();
  });
}

// Call the function to apply the filtering
filterItems();
```

Line 3  
Selects all elements with the class item that are children of an element
with the class list

Lines 8-11  
Gets the `data-id` as a number and hides the item if it’s even.

Lines 14-17  
Retrieves the `data-name` and hides the item if it doesn’t start with
“f”.

Lines 21-24  
Determines the current case of `data-name` (lowercase or uppercase),
sets `newCase` to the opposite case and updates the text content with
the `data-name` in the new case.



> [!WARNING]
>
> To run the code from exercises 4 and 5, a local server must be
> running. The easiest way to do this is to run the project from VSCode
> (or similar) and use an extension like `Live Server`.

## Question 4

> [!NOTE]
>
> See the code in the [question4/](./question4/.) folder

## Question 5

> [!NOTE]
>
> See the code in the [question5/](./question5/.) folder
