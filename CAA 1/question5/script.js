class Product {
  constructor(name, price, quantity, pending = true) {
    this.name = name;
    this.price = price;
    this.quantity = quantity;
    this.pending = pending;
  }

  cost() {
    return this.price * this.quantity;
  }
}

class ProductCategory {
  #name;

  constructor(name) {
    this.#name = name;
    this.productList = [];
  }

  get name() {
    return this.#name;
  }

  set name(newName) {
    this.#name = newName;
  }

  addProduct(product) {
    if (this.productList.some(p => p.name === product.name)) return false;
    this.productList.push(product);
    return true;
  }

  removeProduct(productName) {
    const index = this.productList.findIndex(p => p.name === productName);
    if (index === -1) return false;
    this.productList.splice(index, 1);
    return true;
  }

  getProductByName(productName) {
    return this.productList.find(p => p.name === productName) || false;
  }

  getProducts(pending = false) {
    return pending ? this.productList.filter(p => p.pending) : this.productList;
  }

  getCategoryCost() {
    return this.productList.reduce((total, product) => total + product.cost(), 0);
  }
}

class GroceryList {
  constructor() {
    this.categoryList = [new ProductCategory("default")];
  }

  addCategory(categoryName) {
    if (this.categoryList.some(c => c.name === categoryName)) return false;
    this.categoryList.push(new ProductCategory(categoryName));
    return true;
  }

  add(product, categoryName) {
    let category = this.categoryList.find(c => c.name === categoryName);
    if (!category) {
      this.addCategory(categoryName);
      category = this.categoryList.find(c => c.name === categoryName);
    }
    category.addProduct(product);
  }

  remove(productName) {
    let removed = false;
    this.categoryList.forEach(category => {
      removed = category.removeProduct(productName) || removed;
    });
    return removed;
  }

  search(searchTerm) {
    const results = [];
    this.categoryList.forEach(category => {
      results.push(...category.productList.filter(p => p.name.includes(searchTerm)));
    });
    return results;
  }

  getPendingProducts() {
    const pendingProducts = [];
    this.categoryList.forEach(category => {
      pendingProducts.push(...category.getProducts(true));
    });
    return pendingProducts;
  }

  getTotalCost() {
    return this.categoryList.reduce((total, category) => total + category.getCategoryCost(), 0);
  }

  getMostExpensiveProduct() {
    let mostExpensive = null;
    this.categoryList.forEach(category => {
      category.productList.forEach(product => {
        if (!mostExpensive || product.price > mostExpensive.price) {
          mostExpensive = product;
        }
      });
    });
    return mostExpensive;
  }

  getOverview() {
    return this.categoryList.filter(c => c.productList.length > 0).map(category => ({
      category: category.name,
      nProducts: category.productList.length,
      totalCost: category.getCategoryCost(),
    }));
  }
}
