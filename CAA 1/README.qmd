---
title: "CAA 1"
subtitle: "Web Programming"
author: Alejandro Pérez Bueno
date: today
date-format: "MMM DD, YYYY"
toc: true
toc-title: "Table of Contents"
linkcolor: blue
documentclass: report
link-citations: true
link-bibliography: true
format:
  gfm: default
  pdf:
    linestretch: 1.25
    geometry:
      - top=30mm
      - left=20mm
      - heightrounded
    include-in-header:
      text: |
        \usepackage{fancyhdr}
          \pagestyle{fancy}
          \fancyhead[C]{UOC}
          \fancyhead[R]{aperez-b@uoc.edu}
          \fancyfoot{}
          \fancyfoot[R]{\thepage}
---

{{< pagebreak >}}

## Question 1

```js
let a = 3;
let b = a;
b = 2;

let c = [0, 0];
let d = c;
d[1] = 1;

console.log(a, c[1]);
```

The answer is `c: 3 1`. `a` remains `3` because the change to `b` did not affect its value, since it is a primitive value. On the other hand, `c[1]` is now `1` because the change made through `d` modified the array that both c and d reference. This happens because arrays are objects.

## Question 2

```js
const isMultipleOf = (n, o) => n%o === 0;

console.log(isMultipleOf(35, 5), isMultipleOf('100', '10'), isMultipleOf('a', 'a'))
```

The answer is `b: true true false`. The function takes two arguments, `n` and `o`, which represent the number to be checked and the potential divisor, respectively. If the remainder is equal to zero it returns `true`, otherwise it returns `false`.
* Since `35` is a multiple of `5`, the function returns `true`.
* Javascript is loosely typed, so even though the arguments are strings, they are coerced into numbers before the modulo operation. Since `100` is a multiple of `10`, the function returns `true`.
* `a` is converted to `NaN` (Not a Number). The modulo operation with `NaN` always returns `NaN`, which is not `zero`. Therefore, the function returns `false`.

## Question 3

```js
const data = ['one', 'two', 'three', 'four'];
data.filter((item) => item.charAt(0) === 't'))

console.log(data)
```

The answer is `a: ['one', 'two', 'three', 'four']`. The code defines an array data with the numbers 1, 2, 3 and 4 as strings, and then filters the elements of this array that start with the letter `t` (**t**wo and **t**hree). However the `filter` method does not modify the `data` variable, rather it returns a new array. Thus, the resulting output is the unmodified `data` variable.

## Question 4

The full code would be:

```js
function StudentsAgeAvg(students) {
  if (students.length === 0) { //error management
    return 0;
  }
  const totalAge = students.reduce((sum, student) => sum + student.age, 0);
  return totalAge / students.length;
}

class Student {
  #name;
  #age;
  
  constructor(name, age) {
    this.#name = name;
    this.#age = age;
  }
  
  get age() {
    return this.#age;
  }
}

const studentList = [
  new Student('Leia', 22),
  new Student('Luke', 22),
  new Student('Rey', 20),
  new Student('Anakin', 14),
]

console.log(StudentsAgeAvg(studentList)) //expected output: 19.5
```

The code uses the `reduce` method to iterate over the `student` element from the `students` array, and adds the student's `age` to a `sum` variable, initialized at zero. Then you must simply divide the value of `sum` by the number of students. Of course, to avoy division by zero (if the student list is empty), you must check its length before calling the `reduce` method.

## Question 5

:::{.callout-note}
See the code in the [question5/](./question5/.) folder
:::

:::{.callout-warning}
To run the code, a local server must be running. The easiest way to do this is to run the project from
VSCode (or similar) and use an extension like `Live Server`.
:::
