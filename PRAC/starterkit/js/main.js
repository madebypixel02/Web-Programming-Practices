// js/main.js
import { fetchCategories, fetchSessionToken, fetchQuestion } from './modules/api.js';
import { Randomizer } from './modules/utils/randomizer.js';
import { Encoder } from './modules/utils/encoder.js';

let categories = [];
let sessionToken = '';
let currentQuestionIndex = 0;
let score = 0;
let questions = [];

document.addEventListener('DOMContentLoaded', async () => {
    categories = await fetchCategories();
    sessionToken = await fetchSessionToken();
    setupEventListeners();
    updateRankingUI();
    resetGame();
});

function setupEventListeners() {
    document.querySelector('.js-new').addEventListener('click', startGame);
    document.querySelector('.js-cancel').addEventListener('click', resetGame);
    document.querySelector('.js-save').addEventListener('click', saveScore);
    document.querySelector('.js-discard').addEventListener('click', resetGame);
}

async function startGame() {
    document.querySelector('.js-new').classList.add('d-none');
    document.querySelector('.js-cancel').classList.remove('d-none');
    document.querySelector('.js-score').classList.add('d-none');
    document.querySelector('.js-points').textContent = '0';
    score = 0;
    currentQuestionIndex = 0;
    questions = await fetchQuestions();
    showQuestion();
}

function resetGame() {
    document.querySelector('.js-new').classList.remove('d-none');
    document.querySelector('.js-cancel').classList.add('d-none');
    document.querySelector('.js-score').classList.add('d-none');
    document.querySelector('.js-question').classList.add('d-none');
    document.querySelector('.js-points').textContent = '0';
}

async function fetchQuestions() {
    const questions = [];
    const feedbackElement = document.createElement('p');
    feedbackElement.classList.add('loading-feedback');
    feedbackElement.textContent = 'Loading questions...';
    document.querySelector('.js-game').appendChild(feedbackElement);

    for (let i = 0; i < categories.length; i++) {
        const category = categories[i];
        const question = await fetchQuestion(category.id, sessionToken);
        questions.push(question);
        feedbackElement.textContent = `Loading question ${i + 1} of ${categories.length}...`;
        await new Promise(resolve => setTimeout(resolve, 5000)); // Add this line to introduce a 5-second delay
    }

    document.querySelector('.js-game').removeChild(feedbackElement);
    return questions;
}

function showQuestion() {
    if (currentQuestionIndex >= questions.length) {
        endGame();
        return;
    }

    const question = questions[currentQuestionIndex];
    const questionElement = document.querySelector('.js-question');
    questionElement.classList.remove('d-none');
    questionElement.querySelector('.question__statement').textContent = Encoder.htmlEntitiesDecode(question.question);

    const answers = [question.correct_answer, ...question.incorrect_answers];
    const shuffledAnswers = Randomizer.randomizeArray(answers);

    const answersList = questionElement.querySelector('.question__answers');
    answersList.innerHTML = '';
    shuffledAnswers.forEach(answer => {
        const li = document.createElement('li');
        li.classList.add('question__option');
        li.dataset.value = answer;
        li.textContent = Encoder.htmlEntitiesDecode(answer);
        li.addEventListener('click', () => checkAnswer(answer));
        answersList.appendChild(li);
    });
}

function checkAnswer(selectedAnswer) {
    const question = questions[currentQuestionIndex];
    if (selectedAnswer === question.correct_answer) {
        score += 2;
    }
    document.querySelector('.js-points').textContent = score;
    currentQuestionIndex++;
    showQuestion();
}

function endGame() {
    document.querySelector('.js-question').classList.add('d-none');
    document.querySelector('.js-score').classList.remove('d-none');
}

function saveScore() {
    const username = document.querySelector('.js-username').value.trim();
    if (!username) {
        alert('Please enter a username');
        return;
    }

    const ranking = getRanking();
    ranking.push({ username, score });
    ranking.sort((a, b) => b.score - a.score);
    setRanking(ranking);
    updateRankingUI();
    resetGame();
}

function getRanking() {
    const ranking = document.cookie.split('; ').find(row => row.startsWith('ranking='));
    return ranking ? JSON.parse(decodeURIComponent(ranking.split('=')[1])) : [];
}

function setRanking(ranking) {
    document.cookie = `ranking=${encodeURIComponent(JSON.stringify(ranking))}; path=/; max-age=${60 * 60 * 24 * 365}; SameSite=None; Secure`;
}

function updateRankingUI() {
    const ranking = getRanking();
    const rankingList = document.querySelector('.js-list');
    rankingList.innerHTML = '';

    if (ranking.length === 0) {
        document.querySelector('.js-empty').classList.remove('d-none');
    } else {
        document.querySelector('.js-empty').classList.add('d-none');
        ranking.forEach((record, index) => {
            const li = document.createElement('li');
            li.classList.add('scoreboard__item');
            li.innerHTML = `
                <p class="scoreboard__pos">${index + 1}</p>
                <p class="scoreboard__name">${record.username}</p>
                <p class="scoreboard__points">${record.score} pts</p>
            `;
            rankingList.appendChild(li);
        });
    }
}