// js/modules/api.js
import { CATEGORY_LIST } from './consts/category-list.js';

const API_BASE_URL = 'https://opentdb.com';

async function fetchCategories() {
    const response = await fetch(`${API_BASE_URL}/api_category.php`);
    const data = await response.json();
    return data.trivia_categories.filter(category => CATEGORY_LIST.includes(category.name));
}

async function fetchSessionToken() {
    const response = await fetch(`${API_BASE_URL}/api_token.php?command=request`);
    const data = await response.json();
    return data.token;
}

async function fetchQuestion(categoryId, token) {
    const response = await fetch(`${API_BASE_URL}/api.php?amount=1&category=${categoryId}&token=${token}`);
    const data = await response.json();
    return data.results[0];
}

export { fetchCategories, fetchSessionToken, fetchQuestion };